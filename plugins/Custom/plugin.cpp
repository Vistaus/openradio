#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "custom.h"

void CustomPlugin::registerTypes(const char *uri) {
    //@uri Custom
    qmlRegisterSingletonType<Custom>(uri, 1, 0, "Custom", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Custom; });
}
