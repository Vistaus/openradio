#ifndef CUSTOM_H
#define CUSTOM_H

#include <QObject>
#include <QString>

class Custom: public QObject {
    Q_OBJECT
    Q_PROPERTY(bool isTouch MEMBER isTouch NOTIFY main)
    Q_PROPERTY(QString country MEMBER country NOTIFY main)

public:
    Custom();
    ~Custom() = default;

    Q_INVOKABLE int getDevice();
    Q_INVOKABLE QString getCountry();

signals:
    int main();

private:

  bool isTouch = Custom::getDevice();
  QString country = Custom::getCountry();

};


#endif
