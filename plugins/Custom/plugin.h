#ifndef CUSTOMPLUGIN_H
#define CUSTOMPLUGIN_H

#include <QQmlExtensionPlugin>

class CustomPlugin : public QQmlExtensionPlugin {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif
