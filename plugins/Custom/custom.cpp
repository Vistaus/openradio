#include <QDebug>
#include <QQuickView>
#include <QQuickItem>
#include <QTouchDevice>
#include <QString> 
#include <QLocale>
/*
#include <QDir>
#include <QStandardPaths>
*/
#include "custom.h"

Custom::Custom() {

/*
	QString appDataLocation = QStandardPaths::locate(QStandardPaths::DataLocation, QString(), QStandardPaths::LocateDirectory);    
	QString folder = appDataLocation + "/favorites";
    QDir dir(folder);
    if (!dir.exists())
        dir.mkpath(appDataLocation);
*/
	
}

int Custom::getDevice() {

  bool isTouch = false;
  foreach (const QTouchDevice *dev, QTouchDevice::devices())
      if (dev->type() == QTouchDevice::TouchScreen) {
          isTouch = true;
          break;
      }

  return isTouch;
}

QString Custom::getCountry(){


  //QString c = QLocale::countryToString(QLocale().country());
  QString countryCode = QLocale().name().split('_').at(1);

  const char *code[5] = { "NL", "US", "EN", "FR", "ES"}; 

  if (std::find(std::begin(code), std::end(code), countryCode) != std::end(code))
    //QString country = countryCode;
    return countryCode;
  else
    //QString country = "EN";
    return "EN";


}