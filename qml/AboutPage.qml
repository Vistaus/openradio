/*
 * Copyright (C) 2020  Wproject - Aitzol Berasategi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * OpenRadio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

import "components"

Page {
    id: aboutPage
    visible: false
    header: PageHeader{visible: false}

    Flickable {
        id: aboutContainer

        anchors.fill: parent
        contentHeight: contentCol.height + units.gu(2)
        //anchors.top: header ? header.bottom : parent.top

        width: parent.width; height: parent.height
        //anchors.top: header ? header.bottom : parent.top
        //anchors.top: parent.top
        //anchors.bottom: parent.bottom

        Column {
            id: contentCol

            anchors.topMargin: units.gu(2)
            anchors.top: parent.top
            width: parent.width

            Image {
                id: logoInfo
                source: "../assets/logo.svg"
                sourceSize.width: density <= 10 ? 128 : 350
                sourceSize.height: density <= 10 ? 128 : 350
                anchors.horizontalCenter: parent.horizontalCenter
                verticalAlignment: Image.AlignTop
                fillMode:Image.PreserveAspectFit
            }

            ListItem.Empty{

                highlightWhenPressed: false
                height: aboutTextLayout.height + (divider.visible ? divider.height : 0)
                ListItemLayout {
                    id: aboutTextLayout

                    Label{
                        text: i18n.tr("OpenRadio is a simple application to browse internet radio stations based on the <strong>Community Radio Browser</strong>'s <a href='http://www.radio-browser.info/gui/#!/'>API</a>. The logo has been published at <a href='https://commons.wikimedia.org/wiki/File:OpenRadio_logo.svg'>Wikimedia Commons</a> under <strong>CC BY 4.0</strong> license.")
                        font.pointSize: units.dp(10)
                        anchors.leftMargin: units.gu(2)
                        anchors.left: parent.left
                        anchors.rightMargin: units.gu(2)
                        anchors.right: parent.right
                        horizontalAlignment : Text.AlignJustify
                        wrapMode: Text.WordWrap
                        linkColor: theme.palette.normal.focus
                        onLinkActivated: Qt.openUrlExternally(link)
                    }

                }
            }

            CustomListItem {
                name: i18n.tr("Version")
                content: version
            }

            CustomListItem {
                name: i18n.tr("License")
                content: "GPL-3"
            }

            CustomListItem {
                name: i18n.tr("Source code")
                progressSymbol: true
                onClicked: Qt.openUrlExternally("https://gitlab.com/aitzol76/openradio")
            }

            CustomListItem {
                name: i18n.tr("More Apps")
                progressSymbol: true
                onClicked: Qt.openUrlExternally("https://open-store.io/?sort=relevance&search=author%3AWproject")
            }

            CustomListItem {
                name: i18n.tr("Donate")
                progressSymbol: false
                onClicked: Qt.openUrlExternally("https://liberapay.com/Wproject/donate")
                Icon {
                    name: "like"
                    color: UbuntuColors.red
                    width: units.gu(4)
                    height: units.gu(4)
                    anchors.right: parent.right
                    anchors.rightMargin: units.gu(1.6)
                    anchors.verticalCenter: parent.verticalCenter
                    SlotsLayout.position: SlotsLayout.Trailing
                    //visible: icon != "" || iconSource != "" ? true : false
                    visible: true
                }
            }

        }

        ScrollBar.vertical: ScrollBar {
            width: 10
            parent: aboutContainer.parent
            anchors.top: aboutContainer.top
            anchors.right: aboutContainer.right
            anchors.bottom: aboutContainer.bottom
            policy: ScrollBar.AsNeeded
        }

    }

}
