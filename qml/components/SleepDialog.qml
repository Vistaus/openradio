/*
 * Copyright (C) 2020  Wproject - Aitzol Berasategi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * OpenRadio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2

Dialog {
    id: sleepDialog

    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    parent: ApplicationWindow.overlay
    modal: true
    title: i18n.tr("Adjust sleep time")

    property int t: 5
    property int deadline
    property real currentDate
    property real stopDate

    property alias timer: interval2

    implicitWidth: Math.max(background ? background.implicitWidth : 0,
                            header && header.visible ? header.implicitWidth : 0,
                            footer && footer.visible ? footer.implicitWidth : 0,
                            contentWidth > 0 ? contentWidth + leftPadding + rightPadding : 0)
    implicitHeight: Math.max(background ? background.implicitHeight : 0,
                             (header && header.visible ? header.implicitHeight + spacing : 0)
                              + (footer && footer.visible ? footer.implicitHeight + spacing : 0)
                              + (contentHeight > 0 ? contentHeight + topPadding + bottomPadding : 0))

    contentWidth: contentItem.implicitWidth || (contentChildren.length === 1 ? contentChildren[0].implicitWidth : 0)
    contentHeight: contentItem.implicitHeight || (contentChildren.length === 1 ? contentChildren[0].implicitHeight : 0)


    leftPadding: sleepDialog.Suru.units.gu(2)
    rightPadding: sleepDialog.Suru.units.gu(2)
    topPadding: sleepDialog.Suru.units.gu(1)
    bottomPadding: sleepDialog.Suru.units.gu(1)

    enter: Transition {
        // grow_fade_in
        NumberAnimation { property: "scale"; from: 0.91; to: 1.0; easing: sleepDialog.Suru.animations.EasingIn; duration: sleepDialog.Suru.animations.SnapDuration }
        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0; easing: sleepDialog.Suru.animations.EasingIn; duration: sleepDialog.Suru.animations.SnapDuration }
    }

    exit: Transition {
        // fade_out
        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0; easing: sleepDialog.Suru.animations.EasingIn; duration: sleepDialog.Suru.animations.SnapDuration }
    }


    background: Rectangle {
        color: theme.palette.normal.background
        radius: sleepDialog.Suru.units.dp(4)
       // border.width: 1
      //  border.color: sleepDialog.Suru.neutralColor

        layer.enabled: true
        
        layer.effect: ElevationEffect {
            elevation: 3
        }
    }

    header: Label {
        text: sleepDialog.title
        visible: sleepDialog.title
        elide: Label.ElideRight
        font.pixelSize: units.dp(21)
        color: theme.palette.normal.backgroundText
        topPadding: sleepDialog.Suru.units.gu(3)
        leftPadding: sleepDialog.Suru.units.gu(2)
        rightPadding: sleepDialog.Suru.units.gu(2)

        Suru.textLevel: sleepDialog.HeadingThree
        Suru.textStyle: sleepDialog.PrimaryText
    }

    Column {
        spacing: 40
        width: parent.width

        Label {
            width: parent.width
            wrapMode: Label.Wrap
            font.pixelSize: units.dp(14)
            horizontalAlignment: Qt.AlignHCenter
            text: t + " \'"
            color: theme.palette.normal.backgroundText
        }

        Slider {
            id: slider
            to: 60
            stepSize: 5
            value: 5
            snapMode: Slider.SnapOnRelease
            anchors.horizontalCenter: parent.horizontalCenter
            onMoved: {t = slider.valueAt(position)}

            background: Rectangle {
                x: slider.leftPadding
                y: slider.topPadding + slider.availableHeight / 2 - height / 2
                implicitWidth: 200
                implicitHeight: 4
                width: slider.availableWidth
                height: implicitHeight
                radius: 2
                color: "#bdbebf"

                Rectangle {
                    width: slider.visualPosition * parent.width
                    height: parent.height
                    color: theme.palette.normal.focus
                    radius: 2
                }
            }

            handle: Rectangle {
                x: slider.leftPadding + slider.visualPosition * (slider.availableWidth - width)
                y: slider.topPadding + slider.availableHeight / 2 - height / 2
                implicitWidth: units.gu(2)
                implicitHeight: units.gu(2)
                radius: units.gu(1)
                color: slider.pressed ? "#f0f0f0" : "#f6f6f6"
                border.color: "#bdbebf"
            }
        }


    }

    //buttons on dialog footer
    footer: DialogButtonBox {

        Button {
            id: accept_btn
            text: i18n.tr("SET")
            enabled: t >= 5
            DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole

            contentItem: Text {
                text: accept_btn.text
                font: accept_btn.font
                opacity: enabled ? 1.0 : 0.3
                color: accept_btn.down ? theme.palette.normal.background : theme.palette.normal.backgroundSecondaryText
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }

            background: Rectangle {
                color: accept_btn.down ? theme.palette.normal.focus : (accept_btn.hovered ? theme.palette.normal.foreground : theme.palette.normal.overlaySecondaryText)
                //border.color: theme.palette.normal.foreground
                radius: units.gu(0.4)
            }
            
        }
        Button {
            id: cancel_btn
            text: i18n.tr("CANCEL")
            DialogButtonBox.buttonRole: DialogButtonBox.RejectRole

            contentItem: Text {
                text: cancel_btn.text
                font: cancel_btn.font
                opacity: enabled ? 1.0 : 0.3
                color: cancel_btn.down ? theme.palette.normal.background : theme.palette.normal.backgroundSecondaryText
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }

            background: Rectangle {
                color: cancel_btn.down ? theme.palette.normal.focus : (cancel_btn.hovered ? theme.palette.normal.foreground : theme.palette.normal.overlaySecondaryText)
                //border.color: theme.palette.normal.overlay
                radius: units.gu(0.4)
            }
        }

        //onAccepted: console.log('accept')
        //onRejected: console.log('cancel')
    }

    onAccepted: {
        //t = 1
        deadline = t * 60 * 1000
        //var currentDate = new Date().getTime();
        countdown(deadline)
    }
    function countdown(d){
        currentDate = new Date().getTime();
        console.log(currentDate)
        stopDate = currentDate + d
        console.log(stopDate)
        interval2.start()
    }

    Timer {
        id:interval2
        interval: 1
        running: false
        repeat: true
        onTriggered: count()
    }

    function count(){
        //currentDate++
        sleep = true;
        var cd = new Date().getTime();
        if(cd >= stopDate){
            console.log('Time is out!')
            interval2.stop()
            playRadio.pause()
            Qt.exit(0)
        }else{
            //console.log('d: ' + currentDate)
            //console.log('r: ' + cd)
            var timeDiff = stopDate - new Date().getTime()
            var seconds = Math.floor(timeDiff / 1000);
            var minutes = Math.floor(seconds / 60);

            minutes %= 60;
            seconds %= 60;

            //add a zero before...
            var fmins = ("0" + minutes).slice(-2);
            var fsecs = ("0" + seconds).slice(-2);
            output = fmins + " : " + fsecs
        }
    }

}