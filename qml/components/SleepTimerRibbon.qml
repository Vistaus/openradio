/*
 * Copyright (C) 2020  Wproject - Aitzol Berasategi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * OpenRadio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import QtGraphicalEffects 1.0


/*sleep countdown*/
Item {
    id: sleepBarContainer
    visible: favoritesView
    //anchors.bottom: stackView.currentItem.footer.visible ? stackView.currentItem.footer.top : parent.bottom
    anchors.bottom: stackView.currentItem == page1 ? (stackView.currentItem.footer.visible ? stackView.currentItem.footer.top : parent.bottom) : parent.bottom
    //x: orientation == 1 ? parent.width + (sleepBar.width + sleepBar.width/2) : parent.width
    x: sleep ? parent.width : parent.width + (sleepBar.width + sleepBar.width/2)
    z: 2

    Rectangle {
        z: 2
        id: sleepBar
        visible: sleep
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin : width/2    
        anchors.bottomMargin : width/2
        width: density <= 10 ? 100 : 150
        height: width
        radius: width/2
        color: "#05000A"
        Text {
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: output ? output : "00:00"
            font.family: "Helvetica"
            font.pointSize: 24
            color: output < "01 : 00" ? UbuntuColors.red : "#A8F30A"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: click()
        }

        layer.enabled: true
    
        layer.effect: DropShadow {
            transparentBorder: true
            radius: 2
            horizontalOffset: -1
            verticalOffset: 0
            color: "#62888888"
        }
        
    }

    Rectangle {
        z: 1
        id: sleepBar2
        visible: sleep
        anchors.top: sleepBar.top
        anchors.bottom: sleepBar.bottom
        anchors.leftMargin : -width/2 
        anchors.left: sleepBar.right
        color: "#05000A"
        width: sleepBar.width
        height: sleepBar.height

        Icon {
            name: "delete"
            color: "white"
            anchors.right: parent.right
            anchors.rightMargin: parent.width/16
            anchors.verticalCenter: parent.verticalCenter
            width: density <= 10 ? units.dp(36) : units.dp(24)
            height: width
            //width: units.dp(36)
            //height: units.dp(36)
        }

        MouseArea {
            anchors.fill: parent
            onClicked: click()
        }
    }

    XAnimator {
        id: xAnimation
        target: sleepBarContainer;
        //loops: 1
        from: parent.width + (sleepBar.width + sleepBar.width/2);
        to: parent.width;
        duration: 200
        running: sleep
    }

    /* js */
    function stopSleepTimer(arg){
        arg.timer.stop()
        sleep = false;
    }

    function click(){
        confirmationDialog.params = {
            title: i18n.tr("Stop sleep timer"),
            text: i18n.tr("Do you want to stop sleep timer?"),
            buttonColor: UbuntuColors.green,
            //buttonColor:theme.palette.normal.porcelain,
            arg: sleepDialog,
            func: stopSleepTimer
        }
        confirmationDialog.open();
    }
    
}