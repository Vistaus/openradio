/*
 * Copyright (C) 2020  Wproject - Aitzol Berasategi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * OpenRadio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3
//import QtQuick.Controls.Suru 2.2

ListItem {
    id: customListItem

    property var name: ""
    property var content: ""
    property var icon: ""
    property var iconSource: ""
    property var source: iconSource != "" ? listIcon.source = iconSource : {}
    property var iconColor: UbuntuColors.slate
    property var iconWidth: units.gu(4)
    property var iconHeight: units.gu(4)
    property var dividerColor
    property bool progressSymbol
    height: layout.height + (divider.visible ? divider.height : 0)
    divider.colorFrom: dividerColor ? dividerColor : divider.colorFrom

    ListItemLayout {
        id: layout
        title.text: name

        Label{
            text: content
            color: UbuntuColors.ash
        }
/*
        Icon {
            id:listIcon
            name: icon
            color: iconColor
            width: iconWidth
            height: iconHeight
            SlotsLayout.position: SlotsLayout.Leading
            //visible: icon != "" || iconSource != "" ? true : false
            visible: true
        }

*/

        Image {
            //source: homeTimeLineList.length > 0 ? reTooterAvatar : ""
            source: iconSource
            id:listIcon
            //width: units.gu(2)
            //height: units.gu(2)
            width: iconWidth
            height: iconHeight
            //x: units.gu(3)
            //y: units.gu(3)
            //visible: true
            visible: icon != "" || iconSource != "" ? true : false
            asynchronous: true
            SlotsLayout.position: SlotsLayout.Leading
            onStatusChanged: {
                if(status == Image.Ready){
                    iconSpinner.visible = false
                }
                else if(status == Image.Error ){
                    source = defaultIcon
                } 
            }

            BusyIndicator2{
                id:iconSpinner
                running: true
                visible: iconSpinner.running
                width: units.gu(3)
                height: width
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                color: theme.palette.normal.focus
            }

        }

        ProgressionSlot {
            visible: progressSymbol ? true : false
            SlotsLayout.position: SlotsLayout.Trailing
        }
    }

}
