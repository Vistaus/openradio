/*
 * Copyright (C) 2020  Wproject - Aitzol Berasategi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * OpenRadio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3 as Ubuntu

ToolBar {
	id: mainHeader
    width: root.width

    background: Rectangle {
        implicitWidth: parent.width
        implicitHeight: units.gu(6)
        color: theme.palette.normal.background

        Rectangle {
            width: parent.width
            height: 1
            anchors.bottom: parent.bottom
            color: "transparent"
            border.color: theme.palette.normal.overlaySecondaryText
        }
        
    }

    RowLayout {
        spacing: 20
        anchors.fill: parent

        ToolButton {
            id: menuBtn
            contentItem: Ubuntu.Icon {
                name: "navigation-menu"
            }

            background: Rectangle {
                implicitWidth: units.gu(4)
                implicitHeight: units.gu(4)
                opacity: enabled ? 1 : 0.3
                color: theme.palette.normal.overlaySecondaryText
                //color: Qt.darker("#33333333", menuBtn.enabled && (menuBtn.checked || menuBtn.highlighted) ? 1.5 : 1.0)
                radius: units.gu(0.6)
                visible: menuBtn.down || (menuBtn.enabled && (menuBtn.checked || menuBtn.highlighted))
            }

            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                    listView.currentIndex = -1
                } else {
                    drawer.open()
                }
            }

        }

        Label {
            id: titleLabel
            text: title
            color: theme.palette.normal.baseText
            elide: Label.ElideRight
            horizontalAlignment: Qt.AlignLeft
            verticalAlignment: Qt.AlignVCenter
            Layout.fillWidth: true
        }

        ToolButton {
            id: searchBtn
            contentItem: Ubuntu.Icon {
                name: "search"
            }

            background: Rectangle {
                //implicitWidth: density <= 10 ? 30 : 85
                //implicitHeight: density <= 10 ? 30 : 85

                implicitWidth: units.gu(4)
                implicitHeight: units.gu(4)
                opacity: enabled ? 1 : 0.3
                color: theme.palette.normal.overlaySecondaryText
                //color: Qt.darker("#33333333", searchBtn.enabled && (searchBtn.checked || searchBtn.highlighted) ? 1.5 : 1.0)
                radius: units.gu(0.6)
                visible: searchBtn.down || (searchBtn.enabled && (searchBtn.checked || searchBtn.highlighted))
            }

            onClicked: {
                currentHeader = searchHeader
                favoritesView = false
                listModel = []; //clear listmodel
                //searchHeader.field.item.forceActiveFocus() //force focus on the search field
                //console.log(searchHeader.field.sourceComponent)
                //searchHeader.field.sourceComponent = searchFieldComponent
            }
        }

        ToolButton {
            id: toogleBtn
            contentItem: Ubuntu.Icon {
                name: settings.gridView? "view-list-symbolic" : "view-grid-symbolic"
            }

            background: Rectangle {
                implicitWidth: units.gu(4)
                implicitHeight: units.gu(4)
                opacity: enabled ? 1 : 0.3
                color: theme.palette.normal.overlaySecondaryText
                //color: Qt.darker("#33333333", toogleBtn.enabled && (toogleBtn.checked || toogleBtn.highlighted) ? 1.5 : 1.0)
                radius: units.gu(0.6)
                visible: toogleBtn.down || (toogleBtn.enabled && (toogleBtn.checked || toogleBtn.highlighted))
            }

            onClicked: {
                //iconName: settings.toogleView === "view-list-symbolic" ? settings.toogleView = "view-grid-symbolic" : settings.toogleView = "view-list-symbolic";
                //settings.toogleView === "view-list-symbolic" ? parrilla.visible = true : parrilla.visible = false;
                settings.gridView = !settings.gridView;
            }
        }

    }

}