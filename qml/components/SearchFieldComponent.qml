/*
 * Copyright (C) 2020  Wproject - Aitzol Berasategi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * OpenRadio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components.Themes 1.3
import Ubuntu.Components 1.3 as Ubuntu

Component {
    id: searchFieldComponent
    TextField {
        id: inputField
        placeholderText: plh.text //text from plh, due to the placeholdertextcolor is not customizable under qtquick.controls 2.2
        color: theme.palette.normal.fieldText

        inputMethodHints: Qt.ImhNoPredictiveText
        //anchors.fill: searchFieldComponent
        width: searchHeader.width

        Text {
            id: plh
            text: i18n.tr("Search Stations")
            color: theme.palette.normal.overlaySecondaryText
            anchors.verticalCenter: inputField.verticalCenter
            horizontalAlignment: Text.AlignLeft
            leftPadding: units.gu(1.6)
            elide:Text.ElideRight
            visible: !inputField.text && settings.theme === "Ubuntu.Components.Themes.SuruDark"
            //visible: !inputField.text && !inputField.activeFocus
        }

        background: Rectangle {
            id: bg
            implicitWidth: parent.width
            //implicitHeight: 40
            color: inputField.hovered ? theme.palette.normal.foreground : theme.palette.normal.field
            //color: Qt.lighter(theme.palette.normal.field, inputField.hovered ? 1.5 : 1.0)
            border.color: inputField.activeFocus ? theme.palette.normal.focus : theme.palette.normal.overlaySecondaryText
            radius: units.gu(0.6)
        }

        Image {
            anchors { top: inputField.top; right: inputField.right; margins: units.gu(.6) }
            id: clearText
            fillMode: Image.PreserveAspectFit
            smooth: true;
            visible: inputField.text
            //source: ""
            height: inputField.height - units.gu(.6) * 2
            width: inputField.height - units.gu(.6) * 2

            Ubuntu.Icon{
                name: "clear"
                color: theme.palette.normal.fieldText
                width: parent.width
                height: parent.height
                //visible: false
            }

            MouseArea {
                id: clear
                anchors { horizontalCenter: parent.horizontalCenter; verticalCenter: parent.verticalCenter }
                height: inputField.height; width: inputField.height
                onClicked: {
                    inputField.clear()
                    inputField.forceActiveFocus()
                }
            }
        }

        onTextChanged: {

            if (text.length > 0) {

                listModel = stations
                var entry = text.replace(/\b\w/g, function(l){ return l.toUpperCase() })

                function search(arr, s, name){
                    var matches = [], i, key;

                    for(i = arr.length; i--; )
                        for(key in arr[i] )
                            if(key === name)
                                if( arr[i][key].indexOf(s) > -1 )
                                    matches.unshift( arr[i] ); //push at the beginning of the "matches" array
                                    //console.log(matches);
                                    return matches;
                };

                listModel = search(listModel, entry, 'name');

            } else {

                //listModel = stations
                listModel = []

            }
        }

    }

}