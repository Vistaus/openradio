/*
 * Copyright (C) 2020  Wproject - Aitzol Berasategi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * OpenRadio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2

Dialog {
    id: confirmationDialog

    property var params: {}

    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    parent: ApplicationWindow.overlay

    modal: true
    title: params ? params.title : ""

    implicitWidth: Math.max(background ? background.implicitWidth : 0,
                            header && header.visible ? header.implicitWidth : 0,
                            footer && footer.visible ? footer.implicitWidth : 0,
                            contentWidth > 0 ? contentWidth + leftPadding + rightPadding : 0)
    implicitHeight: Math.max(background ? background.implicitHeight : 0,
                             (header && header.visible ? header.implicitHeight + spacing : 0)
                              + (footer && footer.visible ? footer.implicitHeight + spacing : 0)
                              + (contentHeight > 0 ? contentHeight + topPadding + bottomPadding : 0))

    contentWidth: contentItem.implicitWidth || (contentChildren.length === 1 ? contentChildren[0].implicitWidth : 0)
    contentHeight: contentItem.implicitHeight || (contentChildren.length === 1 ? contentChildren[0].implicitHeight : 0)


    leftPadding: confirmationDialog.Suru.units.gu(2)
    rightPadding: confirmationDialog.Suru.units.gu(2)
    topPadding: confirmationDialog.Suru.units.gu(1)
    bottomPadding: confirmationDialog.Suru.units.gu(1)

    enter: Transition {
        // grow_fade_in
        NumberAnimation { property: "scale"; from: 0.91; to: 1.0; easing: confirmationDialog.Suru.animations.EasingIn; duration: confirmationDialog.Suru.animations.SnapDuration }
        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0; easing: confirmationDialog.Suru.animations.EasingIn; duration: confirmationDialog.Suru.animations.SnapDuration }
    }

    exit: Transition {
        // fade_out
        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0; easing: confirmationDialog.Suru.animations.EasingIn; duration: confirmationDialog.Suru.animations.SnapDuration }
    }


    background: Rectangle {
        color: theme.palette.normal.background
        radius: confirmationDialog.Suru.units.dp(4)
       // border.width: 1
      //  border.color: confirmationDialog.Suru.neutralColor

        layer.enabled: true
        
        layer.effect: ElevationEffect {
            elevation: 3
        }
    }

    header: Label {
        text: confirmationDialog.title
        visible: confirmationDialog.title
        elide: Label.ElideRight
        font.pixelSize: units.dp(21)
        color: theme.palette.normal.backgroundText
        topPadding: confirmationDialog.Suru.units.gu(3)
        leftPadding: confirmationDialog.Suru.units.gu(2)
        rightPadding: confirmationDialog.Suru.units.gu(2)

        Suru.textLevel: confirmationDialog.HeadingThree
        Suru.textStyle: confirmationDialog.PrimaryText
    }

    Column {
        id: column
        spacing: 40
        width: parent.width

        Label {
            width: parent.width
            wrapMode: Label.Wrap
            font.pixelSize: units.dp(14)
            text: params ? params.text : ""
            color: theme.palette.normal.backgroundText
        }
    }

    //buttons on dialog footer
    footer: DialogButtonBox {

        Button {
            id: yes_btn
            text: i18n.tr("YES")
            DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole

            contentItem: Text {
                text: yes_btn.text
                font: yes_btn.font
                color: yes_btn.down ? theme.palette.normal.background : theme.palette.normal.backgroundSecondaryText
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }

            background: Rectangle {
                color: yes_btn.down ? theme.palette.normal.focus : (yes_btn.hovered ? theme.palette.normal.foreground : theme.palette.normal.overlaySecondaryText)
                //border.color: theme.palette.normal.foreground
                radius: units.gu(0.4)
            }
            
        }
        Button {
            id: no_btn
            text: i18n.tr("NO")
            DialogButtonBox.buttonRole: DialogButtonBox.RejectRole

            contentItem: Text {
                text: no_btn.text
                font: no_btn.font
                opacity: enabled ? 1.0 : 0.3
                color: no_btn.down ? theme.palette.normal.background : theme.palette.normal.backgroundSecondaryText
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }

            background: Rectangle {
                color: no_btn.down ? theme.palette.normal.focus : (no_btn.hovered ? theme.palette.normal.foreground : theme.palette.normal.overlaySecondaryText)
                //border.color: theme.palette.normal.overlay
                radius: units.gu(0.4)
            }
        }

    }

    onAccepted:{
        params.func(params.arg)
        confirmationDialog.close()
    }
}