/*
 * Copyright (C) 2020  Wproject - Aitzol Berasategi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * OpenRadio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2

Dialog {
    id: notificationDialog

    property var params: {}

    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    parent: ApplicationWindow.overlay

    modal: true
    title: params ? params.title : ""

    implicitWidth: Math.max(background ? background.implicitWidth : 0,
                            header && header.visible ? header.implicitWidth : 0,
                            footer && footer.visible ? footer.implicitWidth : 0,
                            contentWidth > 0 ? contentWidth + leftPadding + rightPadding : 0)
    implicitHeight: Math.max(background ? background.implicitHeight : 0,
                             (header && header.visible ? header.implicitHeight + spacing : 0)
                              + (footer && footer.visible ? footer.implicitHeight + spacing : 0)
                              + (contentHeight > 0 ? contentHeight + topPadding + bottomPadding : 0))

    contentWidth: contentItem.implicitWidth || (contentChildren.length === 1 ? contentChildren[0].implicitWidth : 0)
    contentHeight: contentItem.implicitHeight || (contentChildren.length === 1 ? contentChildren[0].implicitHeight : 0)


    leftPadding: notificationDialog.Suru.units.gu(2)
    rightPadding: notificationDialog.Suru.units.gu(2)
    topPadding: notificationDialog.Suru.units.gu(1)
    bottomPadding: notificationDialog.Suru.units.gu(1)

    enter: Transition {
        // grow_fade_in
        NumberAnimation { property: "scale"; from: 0.91; to: 1.0; easing: notificationDialog.Suru.animations.EasingIn; duration: notificationDialog.Suru.animations.SnapDuration }
        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0; easing: notificationDialog.Suru.animations.EasingIn; duration: notificationDialog.Suru.animations.SnapDuration }
    }

    exit: Transition {
        // fade_out
        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0; easing: notificationDialog.Suru.animations.EasingIn; duration: notificationDialog.Suru.animations.SnapDuration }
    }


    background: Rectangle {
        color: theme.palette.normal.background
        radius: notificationDialog.Suru.units.dp(4)
       // border.width: 1
      //  border.color: notificationDialog.Suru.neutralColor

        layer.enabled: true
        
        layer.effect: ElevationEffect {
            elevation: 3
        }
    }

    header: Label {
        text: notificationDialog.title
        visible: notificationDialog.title
        elide: Label.ElideRight
        font.pixelSize: units.dp(21)
        color: theme.palette.normal.backgroundText
        topPadding: notificationDialog.Suru.units.gu(3)
        leftPadding: notificationDialog.Suru.units.gu(2)
        rightPadding: notificationDialog.Suru.units.gu(2)

        Suru.textLevel: notificationDialog.HeadingThree
        Suru.textStyle: notificationDialog.PrimaryText
    }
    
    Column {
        id: column
        spacing: 20
        width: parent.width

        Label {
            width: parent.width
            wrapMode: Label.Wrap
            font.pixelSize: units.dp(14)
            text: params ? params.text : ""
            horizontalAlignment: Text.AlignJustify
            color: theme.palette.normal.backgroundText
        }

        CheckBox {
            id: control
            checked: false
            visible: !params ? true : (params.sleep ? params.sleep : false)
            text: i18n.tr("Don't show this message again")
            contentItem: Text {
                leftPadding: control.indicator && !control.mirrored ? control.indicator.width + control.spacing : 0
                rightPadding: control.indicator && control.mirrored ? control.indicator.width + control.spacing : 0

                text: control.text
                font: control.font
                elide: Text.ElideRight
                visible: control.text
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter

                color: theme.palette.normal.backgroundText
            }

            indicator: CheckIndicator2 {
                x: control.text ? (control.mirrored ? control.width - width - control.rightPadding : control.leftPadding) : control.leftPadding + (control.availableWidth - width) / 2
                y: control.topPadding + (control.availableHeight - height) / 2
                control: control
                //color: "green"
            }

        }

    }

    //buttons on dialog footer
    footer: DialogButtonBox {

        Button {
            id: close_btn
            text: i18n.tr("CLOSE")
            DialogButtonBox.buttonRole: DialogButtonBox.RejectRole

            contentItem: Text {
                text: close_btn.text
                font: close_btn.font
                opacity: enabled ? 1.0 : 0.3
                color: close_btn.down ? theme.palette.normal.background : theme.palette.normal.backgroundSecondaryText
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }

            background: Rectangle {
                color: close_btn.down ? theme.palette.normal.focus : (close_btn.hovered ? theme.palette.normal.foreground : theme.palette.normal.overlaySecondaryText)
                //border.color: theme.palette.normal.overlay
                radius: units.gu(0.4)
            }
        }
        
        onRejected: reject()
        function reject(){
            if(control.visible){
                if(control.checked){
                    settings.infoAlert = false
                }
                sleepDialog.open();
            }
        }

    }

}