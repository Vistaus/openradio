/*
 * Copyright (C) 2020  Wproject - Aitzol Berasategi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * OpenRadio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import Ubuntu.Components 1.3 as Ubuntu
import QtMultimedia 5.6
import "components"

Page {
    id: stationsViewPage
    visible: false
    header: PageHeader{visible: false}

    property bool isNowPlayingPage: true
    property bool isLandscapeMode: orientation == 2
    property var logo

    // Reglas landscape
    states: [
        State {
            name: "landscape"
            when: orientation == 2

            PropertyChanges {
                target: blurredBackground
                width: parent.width/2.2
                height: parent.height
            }

            AnchorChanges {
                target: blurredBackground
                anchors {
                    top: parent.top
                    left: parent.left
                    right: undefined
                }
            }

            AnchorChanges {
                target: dataContainer
                anchors {
                    top: parent.top
                    left: blurredBackground.right
                    right: parent.right
                    bottom: parent.bottom
                }
            }
        }
    ]

    Flickable {
        id: stationContainer
        anchors.fill: parent
        width: parent.width; height: parent.height
        //anchors.top: header ? header.bottom : parent.top
        //anchors.top: parent.top
        anchors.bottom: parent.bottom

        BlurredBackground {
            id: blurredBackground
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.right: parent.right
            height: title.lineCount === 1 ? parent.height/2 + units.gu(3): parent.height/2
            //logo: listModel[selectedIndex].favicon
            logo: selectedStation ? selectedStation[2] : ""

            Image {
                id: imgCentro
                width: Math.min(parent.width/2, parent.height)
                height: width
                sourceSize:Qt.size(width, height)
                asynchronous: true
                anchors.centerIn: parent
                source: !blurredBackground.logo ? "" : (blurredBackground.logo.indexOf('..')!=0 ? blurredBackground.logo : blurredBackground.logo.substring(1))
            }
        }

        Item {
            id: dataContainer

            anchors {
                top: blurredBackground.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
                margins: units.gu(2)
                bottomMargin: isLandscapeMode ? units.gu(4) : units.gu(2)
            }

            Label {
                id: title
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                //text: listModel[selectedIndex].name
                text: selectedStation ? selectedStation[0] : ""
                elide: Text.ElideRight
                //fontSize: "large"
                maximumLineCount: 2
                wrapMode: Text.WordWrap
                //color: "white"
            }

            RowLayout{
                id: description
                anchors.left: title.left
                anchors.right: title.right
                anchors.top: title.bottom
                anchors.topMargin: units.gu(1)
                Label {
                    id: dLabel
                    text: selectedStation ? selectedStation[3] : ""
                    wrapMode: Text.WordWrap
                    elide: Text.ElideRight
                    maximumLineCount: 4
                    color: "#959595"
                    Layout.fillWidth: true
                    Layout.maximumHeight: implicitHeight
                }

                Ubuntu.Icon {
                    width: units.gu(2)
                    height: width
                    name: "external-link"
                    Layout.rightMargin : units.gu(1)
                    visible: dLabel.text.indexOf("http") != -1
                    color: theme.palette.normal.focus
                    MouseArea {
                        anchors.fill: parent
                        onClicked: Qt.openUrlExternally(dLabel.text)
                    }

                }
            }

            Row {
                id: controlBtns
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: units.gu(2)

                function sw(index) {

                        selectedStation = [listModel[index].name,listModel[index].url,listModel[index].favicon,listModel[index].homepage]

                        if (playRadio.playbackState == 1){
                            playRadio.source = selectedStation[1];
                            playRadio.play();
                        }else{
                            playRadio.source = selectedStation[1];
                        }

                        nowPlaying = listModel[index].name

                }

                Ubuntu.AbstractButton {
                    id: skipBackwardBtn
                    width: units.gu(6)
                    height: width
                    anchors.verticalCenter: parent.verticalCenter
                    
                    opacity: playRadio.position === 0 ? 0.4 : 1.0
                    onClicked: {

                        var sLength = listModel.length - 1;

                        if(currentIndex >= 1){
                            currentIndex--
                        }else{
                            currentIndex = sLength;
                        };

                        controlBtns.sw(currentIndex);

                    }
                    
                    Row {
                        spacing: units.gu(1)
                        anchors.centerIn: parent

                        Ubuntu.Icon {
                            id: skipBackwardIcon
                            width: units.gu(5)
                            height: width
                            name: "media-skip-backward"
                            //color: "white"
                        }
                    }
                }

                Ubuntu.AbstractButton {
                    id: playBtn
                    width: units.gu(10)
                    height: width
                    opacity: playBtn.pressed ? 0.4 : 1.0
                    onClicked: playRadio.playbackState === MediaPlayer.PlayingState ? playRadio.pause() : playRadio.play()

                    Ubuntu.Icon {
                        id: playIcon
                        width: units.gu(7)
                        height: width
                        anchors.centerIn: parent
                        //color: "white"
                        name: playRadio.playbackState === MediaPlayer.PlayingState ? "media-playback-pause" : "media-playback-start"
                                                                                   
                    }
                }

                Ubuntu.AbstractButton {
                    id: skipForwardBtn
                    width: units.gu(6)
                    height: width
                    anchors.verticalCenter: parent.verticalCenter
                    
                    opacity: playRadio.position === 0 ? 0.4 : 1.0
                    onClicked: {

                        var sLength = listModel.length - 1;

                        if(currentIndex < sLength){
                            currentIndex++
                        }else{
                            currentIndex = 0;
                        };

                        controlBtns.sw(currentIndex);

                    }
                    
                    Row {
                        spacing: units.gu(1)
                        anchors.centerIn: parent

                        Ubuntu.Icon {
                            id: skipForwardIcon
                            width: units.gu(5)
                            height: width
                            name: "media-skip-forward"
                            //color: "white"
                        }
                    }
                }

            }
        }



/***************/

        //---------------Swipe for mouse -------------------
        MouseArea{
            //enabled if device doesn't have a touch screen
            enabled: deviceType === true ? false:true
            anchors.fill: parent
            property point origin
            property point destination
            //property bool ready: false
            signal move(int x, int y)
            signal swipe(string direction)
            onPressed: {
                drag.axis = Drag.XAxis
                origin = Qt.point(mouse.x, mouse.y)
                console.log(origin)
            }
            onReleased: {
                destination =  Qt.point(mouse.x, mouse.y)
                console.log(destination.x)
                
                if(origin.x != destination.x){
                    if(origin.x < destination.x){

                        var sLength = listModel.length - 1;
                        if(currentIndex >= 1){
                            currentIndex--
                        }else{
                            currentIndex = sLength;
                        };
                        controlBtns.sw(currentIndex);

                    }else{

                        var sLength = listModel.length - 1;
                        if(currentIndex < sLength){
                            currentIndex++
                        }else{
                            currentIndex = 0;
                        };
                        controlBtns.sw(currentIndex);

                    }

                }
                
            }
        }
        //-----------------Swipe for touchscreen devices ----------------------
        SwipeArea {
            //enabled if device has a touch screen
            enabled: deviceType === true ? true:false
            anchors.fill: parent
            direction: SwipeArea.Leftwards
            height: units.gu(50)
            //immediateRecognition : true
            onDraggingChanged:  {
                
                if ( dragging ) {
                    var sLength = listModel.length - 1;
                    if(currentIndex < sLength){
                        currentIndex++
                    }else{
                        currentIndex = 0;
                    };
                    controlBtns.sw(currentIndex);
                }
                
            }
        }
        SwipeArea {
            anchors.fill: parent
            direction: SwipeArea.Rightwards
            height: units.gu(50)
            //immediateRecognition : true
            onDraggingChanged:  {
                
                if ( dragging ) {
                    var sLength = listModel.length - 1;
                    if(currentIndex < sLength){
                        currentIndex++
                    }else{
                        currentIndex = 0;
                    };
                    controlBtns.sw(currentIndex);
                }

            }
        }





/***************/


    }

}
