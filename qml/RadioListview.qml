import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtMultimedia 5.6
import QtQuick.Window 2.2
import Qt.labs.settings 1.0

import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

import "components"

Page {
    id: radioListViewPage
    visible: false
    header: PageHeader{visible: false}

    property alias footer: footer

    Flickable {
        id: radioContainer
        width: parent.width
        height: parent.height
        //anchors.top: radioListViewPage.header.bottom
        anchors.top: parent.top
        //anchors.top: sleepBar.visible ? sleepBarContainer.bottom : parent.top
        anchors.bottom: controlPanel.visible ? footer.top : parent.bottom
        z:1

        /*  empty search icon  */
        Item {
            id: emptyList
            width: parent.width
            height: parent.height
            visible: !favoritesView && listModel.length <= 0
            Icon {
                id:img
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                //width: density <= 10 ? 36 : 128
                //height: density <= 10 ? 36 : 128
                width: units.dp(36)
                height: units.dp(36)
                name: "clear"
                //color: theme.palette.normal.overlayText
            }
            Text {
                id: label
                anchors.horizontalCenter: img.horizontalCenter
                anchors.top: img.bottom
                anchors.topMargin: 5
                text: i18n.tr("No results")
                font.pointSize: units.dp(10)
                color: theme.palette.normal.backgroundText
            }
        }

        /*  list view  */
        Component {
            id: listDelegate

                Column {
                    width: parent.width

                    CustomListItem{
                        //text
                        name: listModel[index].name
                        //icon
                        iconSource: listModel[index].favicon.match(/\.(jpeg|jpg|gif|png)$/) == null || !listModel[index].favicon ? defaultIcon : listModel[index].favicon

                        iconWidth: density <= 10 ? 36 : 96
                        iconHeight: iconWidth
                        //progressSymbol: favoritesView ? true : false
                        progressSymbol: false

                        // swipe right for delete
                        leadingActions: favoritesView ? swipeToDelete : null


                        Rectangle{
                            id: status
                            anchors.right: parent.right
                            width: units.gu(0.6)
                            height: parent.height
                            color: UbuntuColors.green
                            //visible: radioList.currentItem
                            visible: favoritesView && playRadio.playbackState === MediaPlayer.PlayingState && listModel[index].name == nowPlaying

                            YAnimator {
                                target: status;
                                loops: Animation.Infinite
                                from: status.height;
                                to: 0;
                                duration: 1800
                                running: true
                            }


                        }

                        ListItemActions {
                            id: swipeToDelete
                            actions: [
                                Action {
                                    iconName: "delete"
                                    onTriggered: { //remove favorite
                                        console.log(index)

                                        confirmationDialog.params = {
                                            title: i18n.tr("Remove station"),
                                            text: i18n.tr("Are you sure you want to remove this station?"),
                                            buttonColor: UbuntuColors.green,
                                            //buttonColor:theme.palette.normal.porcelain,
                                            arg: index,
                                            func: remove
                                        }
                                        confirmationDialog.open();

                                    }
                                }
                            ]
                        }

                        MouseArea {
                            id: events
                            anchors.fill: parent
                            onClicked: click(index)
                        }

                    }

                }

        }

        ListView {
            id:radioList
            anchors.fill: parent
            model: listModel
            delegate: listDelegate
            visible: !settings.gridView
            //highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
            //highlight: highlight
            focus: true
            highlightFollowsCurrentItem : true
        }
    

        /*  grid view  */
        Component{
            id: gridDelegate

            Column {
                width: parent.width
                ListItem.Base {
                    id: gridMode
                    showDivider: false
                    height: radioGrid.cellHeight
                    width: radioGrid.cellWidth

                    Rectangle{
                        id: roundedStatus
                        anchors.right: frame.right
                        anchors.rightMargin: -units.gu(0.9)
                        anchors.top: frame.top
                        anchors.topMargin: -units.gu(0.9)
                        width: units.gu(1.8)
                        height: units.gu(1.8)
                        radius: units.gu(0.9)
                        color: UbuntuColors.green
                        z:1
                        //visible: radioList.currentItem
                        visible: favoritesView && playRadio.playbackState === MediaPlayer.PlayingState && listModel[index].name == nowPlaying
                        ScaleAnimator {
                            target: roundedStatus;
                            loops: Animation.Infinite
                            from: 0.5;
                            to: 1;
                            duration: 1800
                            running: true
                        }
                    }

                    Image {
                        id: frame
                        source: listModel[index].favicon.match(/\.(jpeg|jpg|gif|png)$/) == null || !listModel[index].favicon ? defaultIcon.substring(1) : (listModel[index].favicon.indexOf('..')!=0 ? listModel[index].favicon : listModel[index].favicon.substring(1))

                        /*
                        source: {
                            if(listModel[index].favicon.match(/\.(jpeg|jpg|gif|png)$/) == null || !listModel[index].favicon){
                                source = defaultIcon.substring(1);
                            }else if(listModel[index].favicon.indexOf('..')!=0){
                                listModel[index].favicon;
                            }else{
                                listModel[index].favicon.substring(1)
                            }

                        }*/
                        width: orientation === 1 ? units.gu(11) : units.gu(11.5)
                        height: width
                        sourceSize:Qt.size(width/1.3, height/1.3)
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        onStatusChanged: {
                            if(status == Image.Ready){
                                iconSpinner.visible = false
                            }
                            else if(status == Image.Error ){
                                source = defaultIcon.substring(1)
                            } 
                        }
                        BusyIndicator2{
                            id:iconSpinner
                            running: true
                            visible: iconSpinner.running
                            width: units.gu(3)
                            height: width
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                            color: theme.palette.normal.focus
                        }

                    }

                    Label {
                        text: listModel[index].name
                        width: frame.width
                        //fontSize: "small"
                        //color: "white"
                        elide: Text.ElideRight
                        maximumLineCount: 1
                        anchors.top: frame.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    onClicked: click(index)
                    onPressAndHold: {

                        confirmationDialog.params = {
                            title: i18n.tr("Remove station"),
                            text: i18n.tr("Are you sure you want to remove this station?"),
                            buttonColor: UbuntuColors.green,
                            //buttonColor:theme.palette.normal.porcelain,
                            arg: index,
                            func: remove
                        }
                        confirmationDialog.open();

                    }
                }
            }

        }

        GridView {
            id: radioGrid
            anchors.horizontalCenter: parent.horizontalCenter
            model: listModel
            delegate: gridDelegate
            visible: settings.gridView
            focus: true
            cellWidth: orientation === 1 ? units.gu(14) : units.gu(14.5)
            cellHeight: cellWidth
            //width: orientation === 1 ? Math.min( model.count, 3) * cellWidth : Math.min( model.count, 5) * cellWidth
            width: orientation === 1 ? Math.min( radioGrid.count, 3) * cellWidth : Math.min( radioGrid.count, 5) * cellWidth
            height: parent.height
        }

    }
    

    /*  Control panel  */
    Column {
        id: footer
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        z:2
        Rectangle {
            id: controlPanel
            visible: false
            width: parent.width
            height: density <= 10 ? 100 : 150
            color: "#05000A"

            MouseArea {
                z: -1
                anchors.fill: parent
                onClicked: {
                    headerTitle = i18n.tr("Now Playing")
                    root.header = multiPageHeader
                    stackView.push(page2);
                }
            }

            Image {
                id: stationIcon
                anchors.top: parent.top
                anchors.left: parent.left
                //source: listModel[currentIndex].favicon
                //source: selectedStation[2]
                //source: selectedStation[2].indexOf('..')!=0 ? selectedStation[2] : selectedStation[2].substring(1)
                source: !selectedStation ? "" : (selectedStation[2].indexOf('..')!=0 ? selectedStation[2] : selectedStation[2].substring(1))
                width: parent.height - units.gu(0.25)
                height: width
                sourceSize:Qt.size(width, height)
            }

            Column {
                id: controlPanelCol
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.left: stationIcon.right
                anchors.leftMargin: units.gu(2)

                Label {
                    id: stationName
                    //fontSize: "small"
                    font.weight: Font.Bold
                    width: parent.width - units.gu(6)
                    color: "white"
                    elide: Text.ElideRight
                    maximumLineCount: 2
                    wrapMode: Text.WordWrap
                    //text: listModel[currentIndex].name
                    text: selectedStation ? selectedStation[0] : ""
                }
            }

            AbstractButton {
                id: playButton

                width: units.gu(7)
                height: stationIcon.height
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right

                Rectangle {
                    id: btnShadow
                    anchors.fill: parent
                    color: "white"
                    opacity: 0.1
                    visible: playButton.pressed
                }

                onClicked: playRadio.playbackState === MediaPlayer.PlayingState ? playRadio.pause()
                                                                                : playRadio.play()

                Icon {
                    color: "white"
                    width: units.gu(4)
                    height: width
                    anchors.centerIn: btnShadow
                    name: playRadio.playbackState === MediaPlayer.PlayingState ? "media-playback-pause"
                                                                               : "media-playback-start"
                    opacity: playButton.pressed ? 0.4 : 1.0
                }
            }
        }

    }

    /*  js  */
    function click(index){
        if(!favoritesView){ //search page

            function add(index){
                var picked_favorite = [listModel[index].name,listModel[index].url,listModel[index].favicon,listModel[index].homepage]
                //radioList.currentIndex = index
                //currentIndex = radioList.currentIndex
                items = radioList.count - 1

                python.call('app.main.add_to_favorites',[listModel[index].name,listModel[index].url,listModel[index].favicon,listModel[index].homepage], function(result){
                    var filtered = stations.filter(function(el) { return el.name != listModel[index].name; });
                    stations = filtered;
                    listModel.splice(index, 1)
                    listModel = listModel
                });
            }

            confirmationDialog.params = {
                title: i18n.tr("Add new station"),
                text: i18n.tr("Do you want to add this station?"),
                buttonColor: UbuntuColors.green,
                //buttonColor:theme.palette.normal.porcelain,
                arg: index,
                func: add
            }
            confirmationDialog.open();

        }else{ // favorites page

            selectedStation = []
            radioList.currentIndex = index
            currentIndex = radioList.currentIndex
            nowPlaying = listModel[index].name

            controlPanel.visible = true;

            selectedStation = [listModel[index].name,listModel[index].url,listModel[index].favicon,listModel[index].homepage]
            console.log(selectedStation)
            playRadio.playChannel(index)
            /*
            if(playRadio.playbackState === MediaPlayer.PlayingState){
                playRadio.pause()
            }else{
                //playRadio.play()
                playChannel(currentIndex)
            }
            */

        }
    }
    function remove(index){

        python.call('app.main.remove_favorite',[listModel[index].name], function(result){
            console.log('...deleted!!')
            stations = result
            var filtered = favorites.filter(function(el) { return el.name != listModel[index].name; });
            favorites = filtered;
            listModel.splice(index, 1)
            listModel = listModel

        });

    }
/*
    function stopSleepTimer(arg){
        arg.timer.stop()
        sleep = false;
    }
    */
}
